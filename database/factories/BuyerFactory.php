<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Buyer;
use Faker\Generator as Faker;

$factory->define(Buyer::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'city' => $faker->city,
        'country' =>$faker->country,
        'addressLine' =>$faker->address,
        'phone' => $faker->phoneNumber,
    ];
});
