<?php

use Illuminate\Database\Seeder;

class BuyerOrderSeeder extends Seeder
{
    const BUYER_COUNT = 15;
    const MAX_ORDER_COUNT = 7;
    const MAX_ITEM_COUNT = 10;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = \App\Product::all();

        factory(\App\Buyer::class, self::BUYER_COUNT)->create()
            ->each(function ($buyer) use ($products){
                $buyer->orders()->saveMany(
                    factory(\App\Order::class, rand(1, self::MAX_ORDER_COUNT))
                        ->create(['buyer_id' => $buyer->id])
                        ->each(function ($order) use ($products){
                            $order->items()->saveMany(
                                factory(\App\OrderItem::class, rand(1, self::MAX_ITEM_COUNT))
                                ->make(['order_id' => null])
                                ->each(function ($orderItem) use ($products){
                                    $product = $products->random();
                                    $orderItem->product_id = $product->id;
                                    $orderItem->price = $product->price;
                                    $orderItem->sum = \App\OrderItem::sum($orderItem->price, $orderItem->discount, $orderItem->quantity);
                                })
                            );

                        })
                );
            });
    }
}

