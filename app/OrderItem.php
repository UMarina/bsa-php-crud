<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public static function sum($price, $discount, $quantity)
    {
        return round(($price * (100 - $discount)) / 100 * $quantity);
    }
}
