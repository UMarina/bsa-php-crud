<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $items = [];
        $totalSum = 0;
        foreach ($this->items as $item) {
            $items[] = [
                'productName' => $item->product->name,
                'productQty' => $item->quantity,
                'productPrice' => $item->price/100,
                'productDiscount' => $item->discount,
                'productSum' => $item->sum/100,
            ];
            $totalSum += $item->sum;
        }
        return [
            'orderId' => $this->id,
            'orderDate' => $this->date,
            'orderSum' => $totalSum/100,
            'orderItems' => $items,
            'buyer' => [
                'buyerFullName' => "{$this->buyer->name} {$this->buyer->surname}",
                'buyerAddress' => "{$this->buyer->country} {$this->buyer->city} {$this->buyer->addressLine}",
                'buyerPhone' => $this->buyer->phone
            ]
        ];

    }
}





