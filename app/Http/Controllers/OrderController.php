<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use Illuminate\Http\Response;
use App\Product;
use App\Buyer;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();

        return new Response($orders->map(function ($order){
            return new OrderResource($order);
        }));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Input::all();

        if ($buyer = Buyer::find($data['buyerId'])) {
            $order = new Order;
            $order->buyer_id = $buyer->id;
            $order->date = date('Y-m-d');
            $order->save();
            foreach ($data['orderItems'] as $orderItem) {
                if($product = Product::find($orderItem['productId'])) {
                    $item = new OrderItem();
                    $item->order_id = $order->id;
                    $item->product_id = $product->id;
                    $item->price = $product->price;
                    $item->quantity = $orderItem['productQty'];
                    $item->discount = $orderItem['productDiscount'];
                    $item->sum = $item->sum($item->price, $item->discount, $item->quantity);
                    $item->save();
                }
            }
            return new Response (new OrderResource($order));
        } else {
            return new Response([
                'result' => 'fail',
                'message' => "cannot find buyer with id = {$data['buyerId']}"
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($order = Order::find($id)) {
            return new Response(new OrderResource($order));
        } else {
            return new Response([
                'result' => 'fail',
                'message' => "cannot find order with id = {$id}"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $order = Order::find($id);

        if (!$order) return new Response([
            'result' => 'fail',
            'message' => 'order not found'
        ]);

        $data = Input::all();

        $this->deleteOrderItems($id);

        foreach ($data['orderItems'] as $orderItem) {
            if ($product = Product::find($orderItem['productId'])) {
                $item = new OrderItem();
                $item->order_id = $order->id;
                $item->product_id = $product->id;
                $item->price = $product->price;
                $item->quantity = $orderItem['productQty'];
                $item->discount = $orderItem['productDiscount'];
                $item->sum = $item->sum($item->price, $item->discount, $item->quantity);
                $item->save();
            }
        }

        return new Response(new OrderResource($order));



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if ($this->deleteOrderItems($id) === 'success'){
            $result = Order::destroy($id);
        } else {
            return new Response([
                'result' => 'fail',
                'message' => "cannot remove items from order with id = {$id}"
            ]);
        }
        return new Response(['result' => $result ? 'success' : 'fail']);
    }

    public function deleteOrderItems($orderId)
    {
        $result = OrderItem::where('order_id', $orderId)->delete();
        return $result ? 'success' : 'fail';
    }
}
